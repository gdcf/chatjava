/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.SQLException;
import model.QueryMySql;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GDCF
 */
public class QueryMySqlTest {
    
    public QueryMySqlTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testVerifIfExist()
    {   
        QueryMySql sql = new QueryMySql();
        sql.Insert("insert into login(pc_name,pc_ip,pc_mac,pc_account_user) values('unitTestVerifIfExist','','','')");
        Boolean exist = sql.VerifIfExist("select * from login where pc_name = 'unitTestVerifIfExist'");
        assertTrue(exist);
        sql.Delete("delete from login where pc_name = 'unitTestVerifIfExist'");
        
    }
    
    @Test
    public void testGetIdUser()
    {
        QueryMySql sql = new QueryMySql();
        sql.Insert("insert into login(pc_mac,pc_name,pc_ip,pc_account_user) values('unitTestGetIdUser','','','')");
        String exist = "";
        exist = sql.GetIdUser("unitTestGetIdUser");
        assertNotEquals("", exist);
        sql.Delete("delete from login where pc_mac = 'unitTestGetIdUser'");
    }
    
    @Test
    public void testListPb() throws SQLException
    {
        QueryMySql sql = new QueryMySql();
        sql.Insert("insert into probleme(description,id_login) values('unitTestListPb',0)");
        assertNotNull(sql.listPb());
        sql.Delete("delete from probleme where description = 'unitTestListPb'");
        
    }
}
