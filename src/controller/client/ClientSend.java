/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.client;

import controller.common.Message;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;



public class ClientSend implements Runnable {

    private Socket socket;
    private ObjectOutputStream out;

    public ClientSend(Socket soc, ObjectOutputStream o, String m) {
        try {
            socket = soc;
            out = o;
            Message mess = new Message("client", m);
            out.writeObject(mess);
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(ClientSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
//        try {
//
//            Scanner sc = new Scanner(System.in);
//            while (true) {
//                System.out.print("Votre message >> ");
//                ChatForm.setLblChat("Votre message >> ");
//                String m = sc.nextLine();
//                Message mess = new Message("client", m);
//                out.writeObject(mess);
//                out.flush();
//            }
//        } catch (IOException ex) {
//            System.out.println(ex.getMessage());
//        }
    }

}
