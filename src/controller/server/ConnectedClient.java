/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.server;

import controller.common.Message;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import projetjava1.ProjetJava1;
import vue.UserForm;

/**
 *
 * @author gabriel
 */
public class ConnectedClient implements Runnable {

    private static Integer idCounter = 1;
    private Integer id;
    private Server server;
    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private String name;
    InetAddress addr;

    public ConnectedClient(Server s, Socket soc, String na) throws IOException {
        server = s;
        socket = soc;
        id = idCounter;
        idCounter++;
        out = new ObjectOutputStream(socket.getOutputStream());
        if (id==1)
            name = "Administrateur";
        else
        {
            addr = InetAddress.getLocalHost();
            name = addr.getHostName();
            
            
        }
        
        System.out.println("Nouvelle connexion, id = " + id);
    }

    public void run() {
        try {
            in = new ObjectInputStream(socket.getInputStream());
            boolean isActive = true;
            while (isActive) {
                Message mess = (Message) in.readObject();
                if (mess != null) {
                    mess.setName(name);
                    server.broadcastMessage(mess, id);
                } else {
                    server.disconnectedClient(this);
                    isActive = false;
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void sendMessage(Message mess) throws IOException {
        this.out.writeObject(mess);
        this.out.flush();
    }

    public void closeClient() throws IOException {
        this.in.close();
        this.out.close();
        this.socket.close();
    }

    
    public Integer getId() {
        return id;
    }
}
