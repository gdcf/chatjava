/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller.server;

import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import projetjava1.ProjetJava1;

/**
 *
 * @author myriam.fort
 */
public class Connection implements Runnable{

private Server server;
private static ServerSocket serverSocket;

public Connection(Server s) throws IOException
{
    System.out.println("construct connection");
    server=s;
    serverSocket=new ServerSocket(getServer().getPort());
}

public void run()
{
 while(true)
 {
     Socket sockNewClient;
     try {
        sockNewClient = serverSocket.accept();
        ConnectedClient newClient = new ConnectedClient(server, sockNewClient, ProjetJava1.clientName);
        server.addClient(newClient);
   
        Thread threadNewClient = new Thread(newClient);
        threadNewClient.start();
        
     } catch (IOException ex) {
         System.out.println(ex.getMessage());
     }
     
 }
 }

    /**
     * @return the server
     */
    public Server getServer() {
        return server;
    }

    /**
     * @param server the server to set
     */
    public void setServer(Server server) {
        this.server = server;
    }

    /**
     * @return the serverSocket
     */
    public  ServerSocket getServerSocket() {
        return serverSocket;
    }

    /**
     * @param serverSocket the serverSocket to set
     */
    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }
}
