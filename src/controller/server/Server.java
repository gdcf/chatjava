package controller.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import controller.common.Message;

public class Server {
    
    private List<ConnectedClient> clients;
    private int port;
    
    public Server(int p) {
        System.out.println("server(int p)");
        port = p;
        clients = new ArrayList<ConnectedClient>();
        Thread threadConnection;
        try {
            threadConnection = new Thread(new Connection(this));
            
            threadConnection.start();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void addClient(ConnectedClient newClient) throws IOException {
        this.clients.add(newClient);
    }

    public void broadcastMessage(Message mess, int id) throws IOException {
        for (ConnectedClient client : clients) {
            if (client.getId() != id) {
                client.sendMessage(mess);
            }
        }
    }

    public void disconnectedClient(ConnectedClient discClient) throws IOException {
        //déconnecte le client
        discClient.closeClient();
        //supprime le client de la liste des clients connectés
        clients.remove(discClient);
        //envoi d'un message aux autres clients
        for (ConnectedClient client : clients) {
            client.sendMessage(new Message("server", "Le client "
                    + discClient.getId() + " nous a quitté"));
        }

    }

    public int getPort() {
        return port;
    }
    
    
}
