/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller.common;

import java.io.Serializable;

/**
 *
 * @author gabriel imrane
 */
public class Message implements Serializable{
    private String sender;
    private String content;
    public Message(String s, String c)
    {
        sender=s;
        content=c;
    }
    public String toString()
    {
        return sender + " : " + content;
    }
    public void setName(String s)
    {
        sender=s;
    }


}
