/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.net.NetworkInterface;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class QueryMySql {
    
    String url = "jdbc:mysql://localhost:3306/chatJava?useSSL=false&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
    String user = "root";
    String password = "root";
        
    public Boolean VerifIfExist(String myquery)
    {
        
        
        String query = myquery;
        
        try (Connection con = DriverManager.getConnection(url, user, password);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query)) {
            
           if (rs.next() == false) {
               return false;
               
           }
           else
               return true;
        } catch (SQLException ex) {
            
            Logger lgr = Logger.getLogger(QueryMySql.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            ResultSet rserror = null;
            return true;
        } 
    }
    
    public String GetIdUser(String macAddress)
    {
       
        
        try (Connection con = DriverManager.getConnection(url, user, password);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select id from login where pc_mac='" + macAddress+ "'")) {
            String data = "";
           while (rs.next()) {
                data = rs.getString("id");
           }
           return data;
        } catch (SQLException ex) {
            
            Logger lgr = Logger.getLogger(QueryMySql.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            ResultSet rserror = null;
            return "";
        } 
    }
    
    public DefaultListModel listPb() throws SQLException {
        
        
        
        DefaultListModel model = new DefaultListModel();
     try (Connection con = DriverManager.getConnection(url, user, password);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from probleme join login on probleme.id_login=login.id")) {
            
           while (rs.next()) {
               model.addElement(rs.getString("id") + " - " + rs.getString("description") + " - Nom du pc avec le problème : " + rs.getString("pc_name"));
           }
           return model;
           
        } catch (SQLException ex) {
            
            Logger lgr = Logger.getLogger(QueryMySql.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            ResultSet rserror = null;
            return null;
        } 

  
}
   
    public void Insert (String myquery)
    {
       
        
        String query = myquery;

        try (Connection con = DriverManager.getConnection(url, user, password);
                Statement st = con.createStatement()) {
           
                String sql = query;
                st.executeUpdate(sql);
            

        } catch (SQLException ex) {

            Logger lgr = Logger.getLogger(QueryMySql.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    
    
    public void Delete (String myquery)
    {
       
        
        String query = myquery;

        try (Connection con = DriverManager.getConnection(url, user, password);
                Statement st = con.createStatement()) {
           
                String sql = query;
                st.executeUpdate(sql);
            

        } catch (SQLException ex) {

            Logger lgr = Logger.getLogger(QueryMySql.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}
